"""final URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf.urls import include, url
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView, LogoutView

from aplicaciones.principal.class_view import MenuList, MenuCreate, MenuUpdate, MenuDelete
#, PositionsList, PositionsCreate, PositionsUpdate, PositionsDelete, FinalMenu

urlpatterns = [
    path('admin/', admin.site.urls),
    path('inicio/', MenuList, name='index'),
    path('crear_menu/', MenuCreate.as_view(), name='crear_menu'),
    path('editar_menu/<int:pk>', MenuUpdate.as_view(), name='editar_menu'),
    path('eliminar_menu/<int:pk>', MenuDelete.as_view(), name='eliminar_menu'),
    path('accounts/login',LoginView.as_view(template_name='login.html'),name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('i18n/', include('django.conf.urls.i18n')),
]
