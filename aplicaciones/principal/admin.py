from django.contrib import admin
from .models import Note, menu_local, menu_web

# Register your models here.
admin.site.register(Note)
admin.site.register(menu_local)
admin.site.register(menu_web)