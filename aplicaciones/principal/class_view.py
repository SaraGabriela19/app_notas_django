from django.shortcuts import redirect, render
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.urls import reverse_lazy
from .models import Note, menu_local, menu_web
from .forms import noteForm
from django.utils.translation import gettext as _
from django.http.response import HttpResponse

""""
class MenuList(ListView):
    model = Note
    template_name = 'index.html'

    def get_queryset(self):
        return self.model.objects.all()
"""
def MenuList(request):
    notes = Note.objects.all()
    menu_locals = menu_local.objects.all()
    menu_webs = menu_web.objects.all()
    contexto = {
        'notes': notes,
        'menu_locals': menu_locals,
        'menu_webs' : menu_webs
    }
    return render(request, 'index.html',contexto)

class MenuCreate(CreateView):
    model = Note
    form_class = noteForm
    template_name = 'crear_menu.html'
    success_url = reverse_lazy('index')

class MenuUpdate(UpdateView):
    model = Note
    form_class = noteForm
    template_name = 'crear_menu.html'
    success_url = reverse_lazy('index')   

class MenuDelete(DeleteView):
    model = Note
    template_name = 'verificacion.html'
    success_url = reverse_lazy('index')     



