from django.db import models

class Note(models.Model):
    id = models.AutoField(primary_key= True)
    title = models.CharField(max_length= 250)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)  

    def __str__(self):
        return self.title

class menu_local(models.Model):
    id = models.AutoField(primary_key= True)
    title = models.CharField(max_length= 250)
    status = models.BooleanField()
    section = models.CharField(max_length= 250)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)  

    def __str__(self):
        return self.title

class menu_web(models.Model):
    id = models.AutoField(primary_key= True)
    title = models.CharField(max_length= 250)
    status = models.BooleanField()
    url = models.CharField(max_length= 250)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)  

    def __str__(self):
        return self.title