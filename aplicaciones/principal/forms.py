from django import forms
from .models import Note

class noteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('title', 'description')
        status = forms.BooleanField()
        widgets = {
                    'title': forms.TextInput(attrs={'class': 'form-control'}),
                    'description': forms.Textarea(attrs={'class': 'form-control'}),
                    }



